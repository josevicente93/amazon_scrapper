# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 6 Mar 2016

@author: jose
'''
import scrapy
import re
import mysql.connector
import scrapy.settings

class Crawler(scrapy.Spider):
    #http://www.markhneedham.com/blog/2015/05/21/python-unicodeencodeerror-ascii-codec-cant-encode-character-uxfc-in-position-11-ordinal-not-in-range128/
    #http://doc.scrapy.org/en/latest/intro/tutorial.html#following-links
    #scrapy runspider crawlerPixmania.py

    #DOWNLOAD_DELAY = 2;

    name = 'pixmania'
    allowed_domains = ["pixmania.es"]

    cnx = mysql.connector.connect(user='root', password='pass',
                                  host='127.0.0.1',
                                  database='tfgData')

    cursor = cnx.cursor()

    start_urls = [
        "http://www.pixmania.es/sitemap-8-sp.html"
    ]

    def parse(self, response):
        for section in response.css('.row ul li a::attr(href)').extract():
            chunks = re.search('http://www.pixmania.es/(.*)/(.*)-(\d*)-m.html', section)

            if(not chunks is None):
                auxNumber = re.search('http://www.pixmania.es/(.*)/(.*)-(\d*)-m.html', section).group(3)
                auxFirstSection = re.search('http://www.pixmania.es/(.*)/(.*)-(\d*)-m.html', section).group(1)
                auxSecondSection = re.search('http://www.pixmania.es/(.*)/(.*)-(\d*)-m.html', section).group(2)

                url = "http://www.pixmania.es/"+auxFirstSection+"/"+auxSecondSection+"/"+"xx-xx-xx-xx-brandasc-1-100-page-"+auxNumber+"-m.html"
                yield scrapy.Request(url, callback=self.parse_section)

    def parse_section(self, response):
        print "parse_section >> "+response.url

        sel = scrapy.Selector(response)
        top = int(sel.css("#content > div:nth-child(3) > section > div.row.nosp > p > strong::text").extract()[0][-1])

        print "top >> "+str(top)
        i = 1

        regex = re.search('http://www.pixmania.es/(.*)/(.*)-(\d*)-m.html', response.url)

        auxNumber = regex.group(3)
        auxFirstSection = regex.group(1)
        auxSecondSection = regex.group(2)

        pageArts = []

        print "fuera"
        while (i <= top):
            url = "http://www.pixmania.es/" + auxFirstSection + "/" + auxSecondSection + "/" + "xx-xx-xx-xx-brandasc-"+str(i)+"-100-page-" + auxNumber + "-m.html"
            i = i + 1
            pageArts.append(scrapy.Request(url, callback=self.parse_section_and_articles))

        return pageArts

    def parse_section_and_articles(self, response):
        print "parse_section_and_articles >> " + response.url
        sel = scrapy.Selector(response)

        arts = []

        for product in sel.css(".productAdditional a::attr(href)").extract():
            url = product
            arts.append(scrapy.Request(url, callback=self.parse_article))

        return arts


    def parse_article(self, response):
        print "parse_article >> " + response.url

        sel = scrapy.Selector(response)
        article_url = response.url #

        article_title = sel.css('#desc_refresh > section > h1 > span:nth-child(2)::text')[0].extract().encode("utf-8")
        article_price = sel.css('#desc_refresh > section > div > section.description.col5 > div > div.productPrices.col12 > div > div > ins::attr(content)')[0].extract()
        article_id = re.search('http://www.pixmania.es/(.*)/(.*)/(.*)-a.html', article_url).group(3)
        try:
            article_description = ""
            description = [desc.encode("utf-8") for desc in sel.xpath('//*[@id="description"]/div/div[2]//text()').extract()]

            for desc in description:
                article_description = article_description + desc
        except IndexError:
            article_description = ""

        print "description "+article_description
        categories = sel.css("#content > div.breadcrumb > ul > li > a > span::text").extract()
        article_category = [unicode(category.encode('utf-8'), 'utf-8') for category in categories]

        real_categories = "["
        for cati in article_category:
            print cati
            real_categories = real_categories + cati
            if cati != article_category[-1]:
                real_categories = real_categories + ", "

        real_categories = real_categories + "]"

        item = {}
        item['id'] = article_id
        item['title'] = unicode(article_title, "utf-8")
        item['category'] = real_categories
        item['price'] = article_price
        item['url'] = article_url
        item['description'] = unicode(article_description.encode('utf-8'), "utf-8").replace("u'", '').replace("'", '')

        print item

        Crawler.cursor.execute("INSERT INTO pixmania VALUES ("+article_id+",'"+article_title+"','"+real_categories+"',"+article_price+",'"+article_url+"','"+item['description']+"');")

        return item


    def closed(self, reason):
        Crawler.cursor.execute("commit;")
        Crawler.cnx.close()