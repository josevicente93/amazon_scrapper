# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 6 Mar 2016

@author: jose
'''
import scrapy
import re
import mysql.connector
import scrapy.settings

class Crawler(scrapy.Spider):
    #http://www.markhneedham.com/blog/2015/05/21/python-unicodeencodeerror-ascii-codec-cant-encode-character-uxfc-in-position-11-ordinal-not-in-range128/
    #http://doc.scrapy.org/en/latest/intro/tutorial.html#following-links
    #scrapy runspider crawlerBricor.py

    #DOWNLOAD_DELAY = 2;

    name = 'bricor'
    allowed_domains = ["bricor.es"]

    cnx = mysql.connector.connect(user='root', password='pass',
                                  host='127.0.0.1',
                                  database='tfgData')

    cursor = cnx.cursor()

    start_urls = [
        "http://www.bricor.es/bricor/index.action"
    ]

    def parse(self, response):
        for section in response.css('#menu_bricor li ul li a::attr(href)').extract():
            chunks = re.search("/bricor/producto.action\?idCategoria=(\d*)", section)
            if(not chunks is None):
                sectionId = chunks.group(1)
                url = "http://www.bricor.es/bricor/paginacion.action?idCategoria="+sectionId+"&numPagina=1&tamPagina=100000"
                print ">>>  " + url
                yield scrapy.Request(url, callback=self.parse_section)

    def parse_section(self, response):
        print "parse_section >> "+response.url

        sel = scrapy.Selector(response)


        pageArts = sel.css(".producto_unidad .productos .div_producto span::attr(data-json)").extract()
        arts = []
        i = 0
        urls = sel.css("a::attr(href)").extract()

        for art in pageArts:
            parts = re.search('{"id":"(.*)","brand":"(.*)","price_final":(.*),"name":"(.*)","category":(.*)}', art)
            item = {}
            if (not parts is None):
                item['id'] = parts.group(1)
                item['brand'] = parts.group(2)
                item['price'] = parts.group(3)
                item['name'] = parts.group(4)
                item['category'] = parts.group(5)
                item['url'] = "http://www.bricor.es" + urls[i]
            else:
                try:
                    chunks = re.search('{"id":"(.*)","price_final":(.*),"name":"(.*)","category":(.*)}', art)
                    item['id'] = chunks.group(1)
                    item['brand'] = ""
                    item['price'] = chunks.group(2)
                    item['name'] = chunks.group(3)
                    item['category'] = chunks.group(4)
                    item['url'] = "http://www.bricor.es" + urls[i]
                except:
                    print ">>>  " + art
                    continue

            #if (item['brand'] is None):
            #   item['brand'] = ""

            item = scrapy.Request(item['url'], meta={'arg': item}, callback=self.get_description)

            arts.append(item)
            i = i +1

        return arts

    def get_description(self, response):
        item = response.meta['arg']

        sel = scrapy.Selector(response)
        #descriptions = sel.css(".detalle_datostecnicos").extract()
        article_description = ""

        description = [desc.encode("utf-8") for desc in sel.xpath('// *[ @ id = "tabs-1"] / ul / li //text()').extract()]

        for desc in description:
            article_description = article_description + desc

        item['description'] = article_description

        print "ITEM: "+str(item)

        Crawler.cursor.execute(
            "INSERT INTO bricor VALUES (" + item['id'].replace("'",'') + ",'" + item['name'].replace("'",'') + "','" + item['category'].replace("'",'') + "'," + item[
                'price'].replace("'",'') + ",'" + item['url'].replace("'",'') + "','" + article_description + "','"+item['brand'].replace("'",'')+"');")

        return item


    def closed(self, reason):
        Crawler.cursor.execute("commit;")
        Crawler.cnx.close()