# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
from customers_generator_impl import generate_users, save_users

reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 2 Aug 2016

@author: jose
'''

##############################
####### CONFIGURATION ########
##############################

cfg = {
    "group_1": {
        "seed": "ES",
        "country": "ES"
    },
    "group_2": {
        "seed": "FI",
        "country": "FI"
    },
    "group_3": {
        "seed": "FR",
        "country": "FR"
    },
    "group_4": {
        "seed": "GB",
        "country": "GB"
    },
    "group_5": {
        "seed": "IE",
        "country": "IE"
    },
    "group_6": {
        "seed": "IR",
        "country": "IR"
    },
    "group_7": {
        "seed": "NL",
        "country": "NL"
    },
    "group_8": {
        "seed": "NZ",
        "country": "NZ"
    },
    "group_9": {
        "seed": "TR",
        "country": "TR"
    },
    "group_10": {
        "seed": "US",
        "country": "US"
    }
}

results = 200

##############################
######## MAIN PROGRAM ########
##############################

if __name__ == "__main__":
    for key in cfg:
        group_object = cfg[key]

        print
        print ">>>> GENERATING USERS: COUNTRY %s SEED %s" % (group_object["country"], group_object["seed"])
        print

        users = generate_users(group_object["country"], group_object["seed"], results)

        save_users(users)
