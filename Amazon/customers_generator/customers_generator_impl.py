# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
from httplib import *
import json
from pymongo import MongoClient

reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 2 Aug 2016

@author: jose
'''


def generate_users(country, seed, results=1):
    #https://randomuser.me/documentation

    connection = HTTPConnection("api.randomuser.me")
    connection.request("GET", "/?results=%d&nat=%s&seed=%s&format=json" % (results, country, seed))
    response = connection.getresponse()

    data = response.read()

    if response.status == 200:
        if "error" in data:
            print ">>>>>>> ERROR " + data
            print
            data = []
        else:
            data = json.loads(data)["results"]
    else:
        print ">>>>>>> NOT 200 HTTP STATUS " + data
        data = []

    connection.close()

    return data


def save_users(users):
    for user in users:
        print user

        client = MongoClient('localhost', 27017)

        db = client.tfg_users

        db.users_collection.insert_one(user)

