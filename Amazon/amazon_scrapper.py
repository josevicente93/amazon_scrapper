# -*- encoding: utf-8 -*-
# encoding=utf8

import sqlite3

from scrapy.crawler import CrawlerProcess

from Amazon.scrapper.amazon_sitemap_scrapper import CrawlerStart

VERSION = 1.01
LOG_DB_NAME = "log.db"


def main():
    log_db_conection = sqlite3.connect("log.db")
    log_db_cursor = log_db_conection.cursor()
    log_db_cursor.execute('''CREATE TABLE IF NOT EXISTS logT (error text, url text, section text, version real)''')  # Creo la tabla si no existe
    log_db_conection.commit()
    log_db_conection.close()

    process = CrawlerProcess()
    scrapper_start = CrawlerStart()
    process.crawl(scrapper_start)
    process.start()

if __name__ == "__main__":
    main()
