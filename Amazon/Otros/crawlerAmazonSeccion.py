# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 6 Jul 2016

@author: jose
'''
import scrapy
import scrapy.settings

class Crawler(scrapy.Spider):
    USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"

    name = 'amazon'
    allowed_domains = ["amazon.com"]

    start_urls = [
        #"https://www.amazon.com/s/ref=sr_nr_n_0?fst=as%3Aoff&rh=n%3A283155%2Cn%3A%211000%2Cn%3A1%2Cn%3A173508%2Cn%3A266162%2Cn%3A3564986011&bbn=266162&ie=UTF8&qid=1468080619&rnid=266162&lo=none"
        #"https://www.amazon.com/s/ref=sr_ex_n_1?rh=n%3A283155&bbn=283155&ie=UTF8&qid=1467998776&lo=none&ajr=2"
        #"https://www.amazon.com/s/ref=sr_il_to_stripbooks?fst=as%3Aoff&rh=n%3A283155%2Cn%3A%211000%2Cn%3A1%2Cn%3A173508%2Cn%3A266162%2Cn%3A3564986011&bbn=266162&ie=UTF8&qid=1467998771&lo=none"
    ]

    def parse(self, response):
        subsections_type_1 = response.xpath("//span[@class='sub-categories__list__item__text' and contains(text(), 'See all ')]/..")
        subsections_type_2 = response.css(".categoryRefinementsSection li a")
        subsections_links = subsections_type_1 if not subsections_type_2 else subsections_type_2
        subsections_links.pop()
        if len(subsections_type_2) > 0:
            last_section_components = response.css(".categoryRefinementsSection li")[-1].css("a")
            if len(last_section_components) == 0:
                top_page = int(response.css(".pagnDisabled::text")[0].extract())
                products_links = {}
                products_links['list'] = []
                for page in range(1, top_page):
                    page_url = response.url + "&page=" + str(page)
                    yield scrapy.Request(page_url, callback=self.get_products_urls, dont_filter=True)
            else:
                for section in subsections_links:
                    section = section.xpath('./@href')[0].extract()
                    url = section + "&fap=1"
                    yield scrapy.Request(url, self.recursive_sections_getter, dont_filter=True)
        else:
            for section in subsections_links:
                section = section.xpath('./@href')[0].extract()
                url = section + "&fap=1"
                yield scrapy.Request(url, self.recursive_sections_getter, dont_filter=True)

    def get_products_urls(self, response):
        item = {}

        sel = scrapy.Selector(response)

        possible_product_links_list_1 = sel.css("#mainResults ul.s-result-list li a.s-access-detail-page")
        possible_product_links_list_2 = sel.css("#resultsCol ul li div.a-fixed-left-grid-col div.a-row.a-spacing-small a")

        links_list = possible_product_links_list_1 if not possible_product_links_list_2 else possible_product_links_list_2

        links_list = [link.xpath('./@href')[0].extract() for link in links_list]

        item['list'] = [link.encode("utf-8") for link in links_list if link.startswith("https://www.amazon.com/")]
        return item

    def recursive_sections_getter(self, response):
        return scrapy.Request(response.url, self.parse)



