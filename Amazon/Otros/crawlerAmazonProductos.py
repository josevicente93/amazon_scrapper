# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 19 Jun 2016

@author: jose
'''
import scrapy
import re
import scrapy.settings
import json
import time

import codecs

class Crawler(scrapy.Spider):
    USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"

    name = 'amazon'
    allowed_domains = ["amazon.com"]

    start_urls = [
        #"https://www.amazon.com/George-Martins-Thrones-5-Book-Boxed/dp/0345535529/ref=sr_1_1?ie=UTF8&qid=1467737294&sr=8-1&keywords=game+of+thrones+books"
        #"https://www.amazon.com/Me-Before-You-A-Novel/dp/B00ANR0Y3S/ref=sr_1_2?s=books&ie=UTF8&qid=1467735754&sr=1-2"
        #"https://www.amazon.com/gp/product/B018FK66TU/ref=s9_top_hd_bw_b3AOIXj_g74_i7?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-4&pf_rd_r=9BP81SATQTZBDHZXYYNZ&pf_rd_t=101&pf_rd_p=9153a92f-01be-5cc8-b17f-e0ec43941e88&pf_rd_i=2901953011#postSPF&showDetailTechData=1"
        #"https://www.amazon.com/Laplink-PCmover-Ultimate-10-Use/dp/B008MR37XK/ref=sr_1_1?s=software&ie=UTF8&qid=1467650213&sr=1-1&showDetailTechData=1"
        #"https://www.amazon.com/gp/product/B0050SXKU4/ref=s9_top_hd_bw_b5NIMgl_g63_i1?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-1&pf_rd_r=MXGAPMD09BF81XKGXFZB&pf_rd_t=101&pf_rd_p=2530892402&pf_rd_i=4924897011&showDetailTechData=1"
        #"https://www.amazon.com/XBOX-360-Console-weeds-Design-Decal/dp/B001IK9ATS/ref=sr_1_10?s=videogames&ie=UTF8&qid=1467649296&sr=1-10&showDetailTechData=1"
        #"https://www.amazon.com/Planet-Waves-Winder-String-Cutter/dp/B0002E1G5C/ref=sr_1_5?s=musical-instruments&ie=UTF8&qid=1467649204&sr=1-5&showDetailTechData=1"
        #"https://www.amazon.com/Personalized-Necklace-Custom-Wedding-Bridesmaid/dp/B015HVACEA/ref=sr_1_1?s=handmade&ie=UTF8&qid=1467648720&sr=1-1"
        #"https://www.amazon.com/iCloth-Avionics-Cleaning-like-new-iCA100/dp/B00B8VQ9SC/ref=sr_1_3?s=electronics&ie=UTF8&qid=1467648378&sr=1-3&showDetailTechData=1"
        #"https://www.amazon.com/Mr-Clean-Eraser-Cleaning-8-Count/dp/B001339ZMW/ref=sr_1_3?s=industrial&ie=UTF8&qid=1467647918&sr=1-3&showDetailTechData=1"
        #"https://www.amazon.com/gp/product/B01DGRCIKI/ref=s9_top_hd_bw_bEKkv_g468_i2?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-3&pf_rd_r=TV6357EH6SN0M31PRPJ8&pf_rd_t=101&pf_rd_p=8c7bd5ac-f591-5bd0-bc4f-bf0dcad74a0c&pf_rd_i=3416381&showDetailTechData=1"
        #"https://www.amazon.com/gp/product/B0186MW362/ref=s9_acsd_aas_bw_c_x_2?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-3&pf_rd_r=J1Y5FS4RVWT5TCD8NM4A&pf_rd_t=101&pf_rd_p=2538124002&pf_rd_i=14675152011"
        #"https://www.amazon.com/PlayStation-500GB-Console-Uncharted-Limited-4/dp/B01BEELH52/ref=sr_1_2?s=videogames&ie=UTF8&qid=1467645747&sr=1-2&showDetailTechData=1"
        #"https://www.amazon.com/Amazon-Elements-Wipes-Sensitive-Flip-Top/dp/B00M4M2W1W/ref=s9_acss_bw_cg_aemerch_2a1?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-2&pf_rd_r=SQSK8HCWBPND666XW7YF&pf_rd_t=101&pf_rd_p=2446163362&pf_rd_i=8514636011"
        #"https://www.amazon.com/Kelloggs-Nutri-Grain-Bars-Strawberry-count/dp/B00I8QZ7OW/ref=lp_10657229011_1_11?srs=7301146011&ie=UTF8&qid=1467641149&sr=8-11"
        #"https://www.amazon.com/Earths-Best-Organic-Infant-Formula/dp/B001BM4JLC/ref=lp_16323121_1_3_s_it?s=baby-products&ie=UTF8&qid=1467571501&sr=1-3"
        #"https://www.amazon.com/gp/product/B002H0KZ9M/ref=s9_top_hd_bw_b7ZRcuR_g121_i2?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-4&pf_rd_r=BN6Y4B6N03668KHXV5K7&pf_rd_t=101&pf_rd_p=78036fae-dbd8-5116-8b99-9ce785d43920&pf_rd_i=6936686011"
        #"https://www.amazon.com/D-Link-DCS-932L-Night-Wi-Fi-Camera/dp/B004P8K24W/ref=sr_1_8?s=home-automation&srs=6563140011&ie=UTF8&qid=1467475869&sr=1-8"
        #"https://www.amazon.com/Zinus-Compack-Adjustable-Spring-Mattress/dp/B00IGGJQ6O/ref=lp_3733101_1_1?s=furniture&ie=UTF8&qid=1467475494&sr=1-1"
        #"https://www.amazon.com/Nordic-Ware-Leakproof-Springform-Inch/dp/B000237FSA/ref=lp_289679_1_1?s=kitchen&ie=UTF8&qid=1467475181&sr=1-1"
        #"https://www.amazon.com/Blue-Buffalo-BLUE-Adult-Brown/dp/B000XZBXP8/ref=sr_1_1?s=pet-supplies&ie=UTF8&qid=1467474458&sr=1-1&refinements=p_n_feature_eight_browse-bin%3A3069515011"
        #"https://www.amazon.com/Ginsey-Toilet-Seat-Tool-Kit/dp/B000CD1BOY/ref=pd_rhf_ee_s_cp_3?ie=UTF8&dpID=41DzLbBw98L&dpSrc=sims&preST=_SL500_SR75%2C135_&psc=1&refRID=CE6X4ST611J5G1A6T5PN"
        #"https://www.amazon.com/gp/product/B000LVMJIY/ref=s9_pdx_hd_bw_bk2g7H_g60_i3?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-4&pf_rd_r=6510KBAZS4VN7TARR60T&pf_rd_t=101&pf_rd_p=a915c2e9-0763-509c-b918-ab1d2c3340eb&pf_rd_i=680350011"
        #"https://www.amazon.com/Bemis-1500EC006-Molded-Elongated-Toilet/dp/B0015C9648/ref=pd_sim_sbs_60_5?ie=UTF8&dpID=4122Up7PHoL&dpSrc=sims&preST=_AC_UL160_SR160%2C160_&psc=1&refRID=4Q3KDAQ21QRP5WFY6R1D"
        #"https://www.amazon.com/Bemis-1500EC000-Molded-Elongated-Toilet/dp/B0015C4YFO/ref=pd_sim_sbs_60_10?ie=UTF8&dpID=31VVla5y7NL&dpSrc=sims&preST=_AC_UL160_SR160%2C160_&psc=1&refRID=4Q3KDAQ21QRP5WFY6R1D"
    ]

    def parse(self, response):
        sel = scrapy.Selector(response)

        #Sections (the last section is the actual subsection of the product and the previous one constains the next one and so on
        sects = [re.compile("\\n\s*").sub("", sect).encode("utf-8") for sect in sel.css("#wayfinding-breadcrumbs_feature_div > ul > li > span > a.a-link-normal::text").extract()]
        if not sects:
            possible_sects = sel.xpath('//*[@id="SalesRank"]/ul/li/span[@class="zg_hrsr_ladder"]')
            if possible_sects:
                for sect in possible_sects[0].xpath('./*'):
                    text = sect.xpath('./text()').extract()
                    text = sect.xpath('./a/text()').extract() if not text else text
                    sects.append(text[0].encode("utf-8"))
            else:
                sects = [sect.extract("utf-8") for sect in sel.xpath('//*[@id="wayfinding-breadcrumbs_feature_div"]/ul/li/span/a/text()').extract()]

        #Name
        name = re.compile("\\n\s*\\n\s*\\n\s*\\n\s*\\n\s*").sub("", sel.css("#productTitle::text")[0].extract())
        name = re.compile("\\n\s*\\n\s*\\n\s*").sub("", name).encode("utf-8")

        #Brand
        try:
            brand = re.compile("\s").sub("", sel.css("#brand::text")[0].extract()).encode("utf-8")
        except:
            brand = ""

        #Price (sometimes a discount is applied to the original price)
        price_contender_1 = sel.css("#priceblock_ourprice::text").extract()
        price_contender_2 = sel.css("#priceblock_dealprice::text").extract()
        price_contender_3 = sel.css("#priceblock_saleprice::text").extract()
        price_contender_4 = sel.xpath('//*[@id="priceblock_ourprice"]/*/text()').extract()
        price_contender_5 = sel.css('#tmmSwatches > ul > li.swatchElement.selected span.a-color-price::text').extract()
        price_string = price_contender_2[0].replace("$", "") if len(price_contender_2) > 0 \
                    else (price_contender_1[0].replace("$", "") if len(price_contender_1) > 0
                    else (price_contender_3[0].replace("$", "") if len(price_contender_3) > 0
                    else re.compile("\\n\s*").sub("", price_contender_5[0]).replace("$", "").encode("utf-8"))
                    )
        try:
            try:
                price = [float(price_string)]
            except:
                price = [float(price_aux.encode("utf-8")) for price_aux in price_string.split("-")]
        except:
            price_string = (price_contender_4[1] + "." + price_contender_4[2]).encode("utf-8")
            try:
                price = [float(price_string)]
            except:
                price = [float(price_aux.encode("utf-8")) for price_aux in price_string.split("-")]

        #ID (ASIN standard)
        id = re.search("https://www.amazon.com/.*/dp/(.*)", response.url).group(1).encode("utf-8")

        #URL
        url = response.url

        #Recommendations (ASIN ID of recommended products)
        try:
            people_also_bought = json.loads(sel.xpath('//*[@id="fallbacksession-sims-feature"]/div/@data-a-carousel-options')[0].extract())['ajax']['id_list']
        except:
            people_also_bought = json.loads(sel.xpath('//*[@id="purchase-sims-feature"]/div/@data-a-carousel-options')[0].extract())['ajax']['id_list']
        people_also_bought = [val.encode("utf-8") for val in people_also_bought]

        #Rate
        rate = float(re.search("(\d\.\d*)", sel.css("#acrPopover i > span::text")[0].extract()).group(1))

        #Seller
        sold_by_contender_1 = sel.css("#soldByThirdParty > b::text").extract()
        sold_by_contender_2 = sel.css("#merchant-info a::text")
        try:
            sold_by = sold_by_contender_1[0] if len(sold_by_contender_1) > 0 else (sold_by_contender_2[0].extract() if len(sold_by_contender_2) > 1 else "Amazon.com")
        except:
            sold_by = ""
        sold_by = sold_by.encode("utf-8")

        #Image
        image = ""
        possible_image_1 = sel.css("#landingImage::attr(src)").extract()
        possible_image_2 = sel.css("#main-image::attr(src)").extract()
        possible_image_3 = sel.css("#imgBlkFront::attr(src)").extract()
        if not possible_image_1:
            if not possible_image_2:
                possible_image_3[0].encode("utf-8")
            else:
                image = possible_image_2[0].encode("utf-8")
        else:
            image = possible_image_1[0].encode("utf-8")

        #Description located under the price
        description_bullets = sel.css("#feature-bullets ul li span::text").extract()
        description_bullets = [line.encode("utf-8") for line in description_bullets if not line.isspace() and not line == "to make sure this fits."]

        #Product Description
        description_custom_text = ' '.join([val.encode("utf-8") for val in sel.xpath('//*[@id="productDescription"]/div//text()').extract()])

        #Important Information
        important_information_contender1 = ' '.join([val.encode("utf-8") for val in sel.xpath('//*[@id="importantInformation"]/div/div//text()').extract()])
        important_information_contender2 = ' '.join([val.encode("utf-8") for val in sel.xpath('//*[@id="important-information"]/div//text()').extract()])
        important_information = important_information_contender1 if important_information_contender1 != ' ' else important_information_contender2

        #Product Information
        detailed_information = {}
        for row in sel.css("#prodDetails > div tr"):
            description_title = re.compile("\\n_*").sub("", row.css("th::text")[0].extract().replace(" ", "_").lower()).replace("\t", "").encode("utf-8")
            description_content = re.compile("\\n\s*").sub("", row.css("td::text")[0].extract()).encode("utf-8")
            detailed_information[description_title] = description_content

        if not any(detailed_information):
            for row in sel.css('#detail-bullets > table tr > td .content > ul li'):
                title_list = row.xpath("./b/text()").extract()
                possible_content = [x for x in row.xpath("./text()").extract() if not x.isspace()]

                if not all([cont == "," for cont in possible_content]):
                    content_list = possible_content
                else:
                    content_list = [', '.join([x for x in row.xpath("./a/text()").extract() if not x.isspace()])]

                if len(title_list) > 0 and len(content_list) > 0:
                    description_title = re.compile("\\n\s*").sub("", title_list[0].replace(": ", "").replace(":", "").lower()).replace(" ", "_").encode("utf-8")
                    description_content = re.compile("\\n\s*").sub("", content_list[0]).encode("utf-8")
                    detailed_information[description_title] = description_content

        try:
            detailed_information['shipping_weight'] = detailed_information['shipping_weight'].replace("(", "").encode("utf-8")
        except:
            pass

        try:
            del detailed_information['customer_reviews']
        except:
            pass

        try:
            del detailed_information['best_sellers_rank']
        except:
            pass

        try:
            del detailed_information['average_customer_review']
        except:
            pass

        try:
            del detailed_information['amazon_best_sellers_rank']
        except:
            pass

        #Technical Information
        technical_information = {}
        possible_tech_info_1 = sel.css("#technical-details_feature_div tr")

        rows = possible_tech_info_1
        for row in rows:
            description_title = re.compile("\\n_*").sub("", row.css("th::text")[0].extract().replace(" ", "_").lower()).encode("utf-8")
            description_content = re.compile("\\n\s*").sub("", row.css("td::text")[0].extract()).encode("utf-8")
            technical_information[description_title] = description_content

        #Size and color combinations (ASIN ID of each possible combination)
        try:
            size_and_color = json.loads(re.search(re.compile("{.+}", re.MULTILINE), sel.css("[language~=JavaScript]::text")[0].extract()).group(0))['dimensionValuesDisplayData']
            size_and_color_cleaned = {}
            for key in size_and_color.keys():
                size_and_color_cleaned[key.encode("utf-8")] = [val.encode("utf-8") for val in size_and_color[key]]
        except:
            size_and_color_cleaned = {}

        #Manufacturer Information
        manufacturer_information = {}
        for row in sel.css(".apm-tablemodule-keyvalue"):
            try:
                description_content = re.compile("\\n\s*").sub("", row.css("td.selected span::text")[0].extract()).encode("utf-8")
            except:
                manufacturer_information.clear()
                break
            description_title = re.compile("\\n_*").sub("", row.css("th span::text")[0].extract().replace(" ", "_").lower()).encode("utf-8")
            manufacturer_information[description_title] = description_content

        #Additional Information
        additional_information = ' '.join(sel.xpath('//*/noscript/div//text()').extract()).encode("utf-8")


        item = {}
        item['id'] = id
        item['name'] = name
        item['brand'] = brand
        item['price'] = price
        item['url'] = url
        item['recommendation'] = people_also_bought
        item['rate'] = rate
        item['seller'] = sold_by
        item['size_color_combinations'] = size_and_color_cleaned
        item['image_url'] = image
        item['description_list'] = description_bullets
        item['description_custom_texts'] = description_custom_text
        item['detailed_information'] = detailed_information
        item['timestamp'] = time.time()
        item['sections'] = sects
        item['important_information'] = important_information
        item['technical_information'] = technical_information
        item['manufacturer_information'] = manufacturer_information
        item['additional_information'] = additional_information

        #f = codecs.open("product.json", 'w', "utf-8")
        #f.write(str(item))
        #f.close()

        return item








