# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
'''
Created on 10 Jul 2016

@author: jose
'''
import scrapy
import scrapy.settings

class Crawler(scrapy.Spider):
    USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"

    name = 'amazon'
    allowed_domains = ["amazon.com"]

    start_urls = [
        "https://www.amazon.com/gp/site-directory/"
    ]

    def parse(self, response):
        item = {}
        selection_blocks = response.css("#shopAllLinks tr td")
        selection_blocks = selection_blocks.css("div")
        selection_links = [link.encode("utf-8") for link in selection_blocks.css("ul li a::attr(href)").extract()]
        del selection_links[:73]
        item['selection_links'] = selection_links
        return item




