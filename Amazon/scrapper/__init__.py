# -*- encoding: utf-8 -*-
# encoding=utf8

from scrapy.crawler import CrawlerProcess
from amazon_sitemap_scrapper import CrawlerStart

import sqlite3

from configuration import LOG_DB_NAME


def main():
    log_db_conection = sqlite3.connect(LOG_DB_NAME)
    log_db_cursor = log_db_conection.cursor()
    log_db_cursor.execute('''CREATE TABLE IF NOT EXISTS logT (error text, url text, section text, version real)''')
    log_db_conection.commit()
    log_db_conection.close()

    process = CrawlerProcess()
    scrapper_start = CrawlerStart()
    process.crawl(scrapper_start)
    process.start()

if __name__ == "__main__":
    main()
