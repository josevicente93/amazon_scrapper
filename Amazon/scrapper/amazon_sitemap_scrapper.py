'''
Esta clase es la encargada de parsear los enlaces de las secciones de amazon.com, que se encuentran en el sitemap.
'''

# -*- encoding: utf-8 -*-
# encoding=utf8
import random
import sys
from subprocess import call
import scrapy

from random_user_agent_module import random_user_agent

reload(sys)
sys.setdefaultencoding('utf8')


class CrawlerStart(scrapy.Spider):
    USER_AGENT = random_user_agent()

    RANDOMIZE_DOWNLOAD_DELAY = True

    name = 'amazon_start'
    allowed_domains = ["amazon.com"]

    start_urls = [
        "https://www.amazon.com/gp/site-directory/"
    ]

    def parse(self, response):
        item = {}

        selection_links = ([link.encode("utf-8")
                            for link in response.xpath('//*[@class="fsdDeptCol"]/a/@href').extract()])

        # Se eliminan links que sabemos que no van a funcionar
        del selection_links[:73]
        del selection_links[-29:]
        # Se hace que el orden de los enlaces sea aleatorio para que cada vez que se ejecute el script
        # haya menos posibilidades de que empiece a analizar siempre los mismos enlaces
        random.shuffle(selection_links)

        item['selection_links'] = selection_links

        for link in item['selection_links']:
            (call(["scrapy", "runspider", "amazon_sections_scrapper.py",
                   "-a", "start_url=https://www.amazon.com%s" % link]))

        return item


















