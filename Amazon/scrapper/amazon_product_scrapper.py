'''
Esta clase tiene como objetivo extraer la informacion de un producto.
'''

# -*- encoding: utf-8 -*-
# encoding=utf8

import ujson
import re
import sqlite3
import time

import scrapy
import configuration
from pymongo import MongoClient

from random_user_agent_module import random_user_agent

import traceback

import logging


class CrawlerProducts(scrapy.Spider):
    USER_AGENT = random_user_agent()

    RANDOMIZE_DOWNLOAD_DELAY = True

    name = 'amazon_product'
    allowed_domains = ["amazon.com"]

    start_urls = []

    def __init__(self, *args, **kwargs):
        super(CrawlerProducts, self).__init__(*args, **kwargs)

        self.start_urls = [kwargs.get('start_url')]

    def parse(self, response):
        # ID (ASIN standard)
        idd = re.search("https://www.amazon.com/.*/dp/(.*)", response.url).group(1).encode("utf-8")

        client = MongoClient(configuration.MONGODB_ADDRESS, configuration.MONGODB_PORT)

        db = client.amazon_products

        item = {}

        if db.products_collection.find({'_id': idd}).limit(1).count() == 0:
            try:
                sel = scrapy.Selector(response)

                # Sections (the last section is the actual subsection of the product and the previous one contains
                # the next one and so on
                sections = CrawlerProducts.get_product_sections(sel)

                # Name
                name = CrawlerProducts.get_product_name(sel)

                # Brand
                brand = CrawlerProducts.get_product_brand(sel)

                # Price (sometimes a discount is applied to the original price)
                price = CrawlerProducts.get_product_price(sel)

                # URL
                url = response.url

                # Recommendations (ASIN IDs of recommended products)
                people_also_bought = CrawlerProducts.get_product_recommendations(sel)

                # Rate
                rate = CrawlerProducts.get_product_rate(sel)

                # Seller
                sold_by = CrawlerProducts.get_product_seller(sel)

                # Image
                image = CrawlerProducts.get_product_seller(sel)

                # Description located under the price
                description_bullets = CrawlerProducts.get_product_description_bullets(sel)

                # Product Description
                description_custom_text = (' '.join([val.encode("utf-8") for val in
                                                     sel.xpath('//*[@id="productDescription"]/div//text()').extract()]))

                # Important Information
                important_information = CrawlerProducts.get_product_important_info(sel)

                # Product Information
                detailed_information = CrawlerProducts.get_product_detailed_info(sel)

                # Technical Information
                technical_information = CrawlerProducts.get_product_tech_info(sel)

                # Size and color combinations (ASIN IDs of each possible combination)
                size_and_color = CrawlerProducts.get_product_size_and_color(sel)

                # Manufacturer Information
                manufacturer_information = CrawlerProducts.get_product_manufacturer_info(sel)

                # Additional Information
                additional_information = ' '.join(sel.xpath('//*/noscript/div//text()').extract()).encode("utf-8")

                item['_id'] = idd
                item['name'] = name
                item['brand'] = brand
                item['price'] = price
                item['url'] = url
                item['recommendation'] = people_also_bought
                item['rate'] = rate
                item['seller'] = sold_by
                item['product_combinations'] = size_and_color
                item['image_url'] = image
                item['description_list'] = description_bullets
                item['description_custom_texts'] = description_custom_text
                item['detailed_information'] = detailed_information
                item['timestamp'] = time.time()
                item['sections'] = sections
                item['important_information'] = important_information
                item['technical_information'] = technical_information
                item['manufacturer_information'] = manufacturer_information
                item['additional_information'] = additional_information

                db.products_collection.insert_one(item)
            except:
                log_db_conection = sqlite3.connect(configuration.LOG_DB_NAME)
                log_db_cursor = log_db_conection.cursor()

                lines_list = traceback.format_exc().splitlines()
                logging.error(str(lines_list))
                limit = len(lines_list)
                error = lines_list[limit - 3].replace("\"", "")

                try:
                    log_db_cursor.execute(
                        "INSERT INTO logT VALUES (\'" + error + "\',\'" + response.url + "\',\'" +
                        str(sections).replace("[", "").replace("]", "").replace(",", "-").replace("'", "")
                        + "\',\'" + str(configuration.VERSION) + "\')")
                except sqlite3.Error:
                    log_db_cursor.execute(
                        "INSERT INTO logT VALUES (\'" + error + "\',\'" + response.url + "\',\'\',\'" +
                        str(configuration.VERSION) + "\')")

                log_db_conection.commit()

                log_db_conection.close()

        client.close()

        return item

    '''
    Devuelve una lista de los nombres de las secciones a las que pertenece un producto.
    '''
    @staticmethod
    def get_product_sections(selector):
        sections = ([re.compile("\\n\s*").sub("", sect).encode("utf-8")
                     for sect in
                     selector.css("#wayfinding-breadcrumbs_feature_div > ul > li > span > a.a-link-normal::text")
                    .extract()]
                    )

        if not sections:
            possible_sections = selector.xpath('//*[@id="SalesRank"]/ul/li/span[@class="zg_hrsr_ladder"]')
            if possible_sections:
                for sect in possible_sections[0].xpath('./*'):
                    text = sect.xpath('./text()').extract()
                    text = sect.xpath('./a/text()').extract() if not text else text
                    sections.append(text[0].encode("utf-8"))

                return sections
            else:
                return ([sect.extract("utf-8") for sect in
                         selector.xpath('//*[@id="wayfinding-breadcrumbs_feature_div"]/ul/li/span/a/text()')
                        .extract()])

    '''
    Devuelve el nombre del producto
    '''
    @staticmethod
    def get_product_name(selector):
        name_contender_1 = selector.css("#productTitle::text")

        if len(name_contender_1) > 0:
            name_contender_1_aux = (re.compile("\\n\s*\\n\s*\\n\s*\\n\s*\\n\s*")
                                    .sub("", name_contender_1[0].extract())
                                    )
            return re.compile("\\n\s*\\n\s*\\n\s*").sub("", name_contender_1_aux).encode("utf-8")
        else:
            return selector.css("#ebooksProductTitle::text")[0].extract().encode("utf-8")

    '''
    Devuelve la marca del producto
    '''
    @staticmethod
    def get_product_brand(selector):
        try:
            return re.compile("\s").sub("", selector.css("#brand::text")[0].extract()).encode("utf-8")
        except (re.error, IndexError):
            return ""

    '''
    Devuelve una lista de posibles precios del producto, ya que a veces un producto puede tener distintos precios
    en funcion de si tiene descuentos o hay varias variantes de ese producto.
    '''
    @staticmethod
    def get_product_price(selector):
        price_contender_1 = selector.css("#priceblock_ourprice::text").extract()
        price_contender_2 = selector.css("#priceblock_dealprice::text").extract()
        price_contender_3 = selector.css("#priceblock_saleprice::text").extract()
        price_contender_4 = selector.xpath('//*[@id="priceblock_ourprice"]/*/text()').extract()
        price_contender_5 = (selector.css('#tmmSwatches > ul > li.swatchElement.selected span.a-color-price::text')
                             .extract())
        price_contender_6 = selector.css('span.a-size-medium.a-color-price.header-price::text').extract()
        price_contender_7 = selector.css('#a-autoid-1-announce > span.a-color-base::text').extract()
        price_contender_8 = selector.css('#a-autoid-2-announce > span.a-color-base::text').extract()
        price_contender_9 = selector.css('#a-autoid-0-announce > span.a-color-base > span::text').extract()

        if len(price_contender_2) > 0:
            price_string = price_contender_2[0].replace("$", "")
        elif len(price_contender_1) > 0:
            price_string = price_contender_1[0].replace("$", "")
        elif len(price_contender_3) > 0:
            price_string = price_contender_3[0].replace("$", "")
        elif len(price_contender_5) > 0:
            price_string = re.compile("\\n\s*").sub("", price_contender_5[0]).replace("$", "")
        elif len(price_contender_6) > 0:
            price_string = re.compile("\\n\s*").sub("", price_contender_6[0]).replace("$", "").encode("utf-8")
        elif len(price_contender_7) > 0:
            price_string = (re.compile("\\n\s*").sub("", price_contender_7[0]).replace("$", "")
                            .replace("from", "")
                            .encode("utf-8"))
        elif len(price_contender_8) > 0:
            price_string = (re.compile("\\n\s*").sub("", price_contender_8[0]).replace("$", "")
                            .replace("from", "")
                            .encode("utf-8"))
        else:
            price_string = (re.compile("\\n\s*").sub("", price_contender_9[0]).replace("$", "")
                            .replace("from", "")
                            .encode("utf-8"))

        try:
            try:
                return [float(price_string)]
            except ValueError:
                try:
                    return [float(price_aux.encode("utf-8")) for price_aux in price_string.split("-")]
                except ValueError:
                    return [float(price_string.replace(",", ""))]
        except ValueError:
            price_string = (price_contender_4[1] + "." + price_contender_4[2]).encode("utf-8")
            try:
                return [float(price_string)]
            except ValueError:
                return [float(price_aux.encode("utf-8")) for price_aux in price_string.split("-")]

    '''
    Devuelve la recomendacion de productos que se hacen a partir de uno
    '''
    @staticmethod
    def get_product_recommendations(selector):
        try:
            people_also_bought = (ujson.loads(
                selector.xpath('//*[@id="fallbacksession-sims-feature"]/div/@data-a-carousel-options')[0].extract()
            )['ajax']['id_list'])
        except IndexError:
            people_also_bought = (ujson.loads(
                selector.xpath('//*[@id="purchase-sims-feature"]/div/@data-a-carousel-options')[0].extract()
            )['ajax']['id_list'])

        return [val.encode("utf-8") for val in people_also_bought]

    '''
    Devuelve la valoracion de un producto
    '''
    @staticmethod
    def get_product_rate(selector):
        try:
            return float(re.search("(\d\.\d*)", selector.css("#acrPopover i > span::text")[0].extract()).group(1))
        except ValueError:
            return 0.0

    '''
    Devuelve el nombre del vendedor de un producto
    '''
    @staticmethod
    def get_product_seller(selector):
        sold_by_contender_1 = selector.css("#soldByThirdParty > b::text").extract()
        sold_by_contender_2 = selector.css("#merchant-info a::text")
        try:
            sold_by = (sold_by_contender_1[0] if len(sold_by_contender_1) > 0
                       else (sold_by_contender_2[0].extract() if len(sold_by_contender_2) > 1 else "Amazon.com")
                       )
        except IndexError:
            sold_by = ""

        return sold_by.encode("utf-8")

    '''
    Devuelve la url de la imagen de producto.
    '''
    @staticmethod
    def get_product_image(selector):
        possible_image_1 = selector.css("#landingImage::attr(src)").extract()
        possible_image_2 = selector.css("#main-image::attr(src)").extract()
        possible_image_3 = selector.css("#imgBlkFront::attr(src)").extract()
        possible_image_4 = selector.css("#ebooksImgBlkFront::attr(src)").extract()

        if not possible_image_1:
            if not possible_image_2:
                if not possible_image_3:
                    if possible_image_4:
                        return possible_image_4[0].encode("utf-8")
                    else:
                        return ''
                else:
                    return possible_image_3[0].encode("utf-8")
            else:
                return possible_image_2[0].encode("utf-8")
        else:
            return possible_image_1[0].encode("utf-8")

    '''
    Devuelve una lista de la lista de descripciones breves del producto.
    '''
    @staticmethod
    def get_product_description_bullets(selector):
        description_bullets = selector.css("#feature-bullets ul li span::text").extract()

        return ([line.encode("utf-8") for line in description_bullets
                if not line.isspace() and not line == "to make sure this fits."])

    '''
    Devuelve la descripcion de producto.
    '''
    @staticmethod
    def get_product_important_info(selector):
        important_information_contender1 = (' '.join([val.encode("utf-8") for val in
                                                      selector
                                                     .xpath('//*[@id="importantInformation"]/div/div//text()')
                                                     .extract()]))
        important_information_contender2 = (' '.join([val.encode("utf-8") for val in
                                                      selector
                                                     .xpath('//*[@id="important-information"]/div//text()')
                                                     .extract()]))
        return (important_information_contender1 if important_information_contender1 != ' '
                else important_information_contender2)

    '''
    Devuelve informacion detallada del producto.
    '''
    @staticmethod
    def get_product_detailed_info(selector):
        detailed_information = {}
        for row in selector.css("#prodDetails > div tr"):
            description_title = (re.compile("\\n_*").sub("", row.css("th::text")[0].extract()
                                                         .replace(" ", "_").lower())
                                 .replace("\t", "").encode("utf-8"))
            description_content = re.compile("\\n\s*").sub("", row.css("td::text")[0].extract()).encode("utf-8")
            detailed_information[description_title.replace(".", "")] = description_content

        if not any(detailed_information):
            for row in selector.css('#detail-bullets > table tr > td .content > ul li'):
                title_list = row.xpath("./b/text()").extract()
                possible_content = [x for x in row.xpath("./text()").extract() if not x.isspace()]

                if not all([cont == "," for cont in possible_content]):
                    content_list = possible_content
                else:
                    content_list = ([', '.join([x for x in row.xpath("./a/text()").extract()
                                                if not x.isspace()])])

                if len(title_list) > 0 and len(content_list) > 0:
                    description_title = (re.compile("\\n\s*").sub("", title_list[0].replace(": ", "")
                                                                  .replace(":", "")
                                                                  .lower()).replace(" ", "_").encode("utf-8"))
                    description_content = re.compile("\\n\s*").sub("", content_list[0]).encode("utf-8")
                    detailed_information[description_title.replace(".", "")] = description_content

        # deleting fields we don't want
        keys = detailed_information.keys()

        if 'shipping_weight' in keys:
            detailed_information['shipping_weight'] = (detailed_information['shipping_weight']
                                                       .replace("(", "").encode("utf-8"))

        if 'customer_reviews' in keys:
            del detailed_information['customer_reviews']

        if 'best_sellers_rank' in keys:
            del detailed_information['best_sellers_rank']

        if 'average_customer_review' in keys:
            del detailed_information['average_customer_review']

        if 'amazon_best_sellers_rank' in keys:
            del detailed_information['amazon_best_sellers_rank']

        return detailed_information

    '''
    Devuelve informacion tecnica sobre el producto
    '''
    @staticmethod
    def get_product_tech_info(selector):
        technical_information = {}
        possible_tech_info_1 = selector.css("#technical-details_feature_div tr")

        rows = possible_tech_info_1
        for row in rows:
            description_title = (re.compile("\\n_*").sub("", row.css("th::text")[0].extract().replace(" ", "_")
                                                         .lower()).encode("utf-8"))
            description_content = re.compile("\\n\s*").sub("", row.css("td::text")[0].extract()).encode("utf-8")
            technical_information[description_title.replace(".", "")] = description_content

        return technical_information

    '''
    Obtiene las combinaciones de color y tamano de cada producto
    '''
    @staticmethod
    def get_product_size_and_color(selector):
        try:
            size_and_color = (ujson.loads(re.search(re.compile("{.+}", re.MULTILINE),
                                                    selector.css("[language~=JavaScript]::text")[0].extract())
                                          .group(0))['dimensionValuesDisplayData'])
            size_and_color_cleaned = {}
            for key in size_and_color.keys():
                size_and_color_cleaned[key.replace(".", "").encode("utf-8")] = ([val.encode("utf-8") for val in
                                                                                 size_and_color[key]])

            return size_and_color_cleaned
        except IndexError:
            return {}

    '''
    Devuelve informacion sobre el manufacturador del producto
    '''
    @staticmethod
    def get_product_manufacturer_info(selector):
        manufacturer_information = {}
        for row in selector.css(".apm-tablemodule-keyvalue"):
            try:
                description_content = (re.compile("\\n\s*").sub("", row.css("td.selected span::text")[0]
                                                                .extract()).encode("utf-8"))
            except IndexError:
                manufacturer_information.clear()
                break
            description_title = (re.compile("\\n_*").sub("", row.css("th span::text")[0].extract()
                                                         .replace(" ", "_").lower()).encode("utf-8"))
            manufacturer_information[description_title.replace(".", "")] = description_content

        return manufacturer_information
