'''
Genera user agents de manera aleatoria a partir de una api online para camuflar las peticiones
'''

# -*- encoding: utf-8 -*-
# encoding=utf8
import sys
from httplib import *
import ujson

reload(sys)
sys.setdefaultencoding('utf8')


def random_user_agent():
    connection = HTTPConnection("api.useragent.io")
    connection.request("GET", "/")
    response = connection.getresponse()

    data = response.read()

    if response.status == 200:
        if "error" in data:
            print ">>>>>>> ERROR " + data
            data = []
        else:
            data = ujson.loads(data)["ua"]
    else:
        print ">>>>>>> NOT 200 HTTP STATUS " + data
        data = []

    connection.close()

    return data


