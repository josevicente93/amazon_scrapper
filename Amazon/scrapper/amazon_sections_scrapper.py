'''
Esta clase se encarga de parsear las secciones de amazon.com, analizando tambien subsecciones, de tal manera que llega
a una subseccion que no tiene subsecciones y en ese caso se procede a analizar cada producto en las distintas paginas de
productos (un maximo de 10 paginas).
'''

# -*- encoding: utf-8 -*-
# encoding=utf8

import re
from subprocess import call
import scrapy

from random_user_agent_module import random_user_agent

import logging


class CrawlerSections(scrapy.Spider):
    USER_AGENT = random_user_agent()

    RANDOMIZE_DOWNLOAD_DELAY = True

    name = 'amazon_sections'
    allowed_domains = ["amazon.com"]

    start_urls = []

    def __init__(self, *args, **kwargs):
        super(CrawlerSections, self).__init__(*args, **kwargs)
        self.start_urls = [kwargs.get('start_url')]

    def parse(self, response):
        subsections_dict = CrawlerSections.get_subsections_urls(response)

        subsections_links = subsections_dict['subsections']
        subsections_type_1 = subsections_dict['type1']
        subsections_type_2 = subsections_dict['type2']

        if len(subsections_links) > 0:
            subsections_links.pop()

            if subsections_type_2:
                deepest_subsection = len(response.css(".categoryRefinementsSection li")[-1].css("a")) == 0

                if deepest_subsection:
                    top_page = CrawlerSections.get_top_page(response)

                    if top_page > 0:
                        pages_base_url = (re.compile("&page=\d+")
                                          .sub("", response.xpath(
                                            '//*[@id="pagn"]/span[@class="pagnLink"]/a[position()=last()]')
                                                [-1].xpath('./@href')[0].extract()).encode("utf-8"))

                        pages_base_url = re.compile("ref=sr_pg_\d+\?").sub("", pages_base_url)

                        # top_page number limited to 10 in case there are more pages
                        top_page = 10 if top_page > 10 else top_page

                        for page in range(1, (top_page + 1)):
                            page_url = "https://www.amazon.com" + pages_base_url + "&page=" + str(page)
                            logging.info("Section Page %d/%d" % (page, top_page))
                            self.USER_AGENT = random_user_agent()
                            yield scrapy.Request(page_url, callback=self.get_products_urls, dont_filter=True)
                    else:
                        self.USER_AGENT = random_user_agent()
                        yield scrapy.Request(response.url, callback=self.get_products_urls, dont_filter=True)

                else:
                    for section in subsections_links:
                        section = section.xpath('./@href')[0].extract()

                        url = section if section.startswith(
                            "https://www.amazon.com") else "https://www.amazon.com" + section
                        self.USER_AGENT = random_user_agent()
                        yield scrapy.Request(url, self.recursive_sections_getter, dont_filter=True)
            elif subsections_type_1:
                for section in subsections_links:
                    section = section.xpath('./@href')[0].extract()
                    url = (section if section.startswith("https://www.amazon.com")
                           else "https://www.amazon.com" + section)
                    self.USER_AGENT = random_user_agent()
                    yield scrapy.Request(url, self.recursive_sections_getter, dont_filter=True)

    '''
    Obtiene subsecciones de una seccion (o subseccion).
    '''
    @staticmethod
    def get_subsections_urls(response):
        subsections_type_1 = (response.xpath(
            "//span[@class='sub-categories__list__item__text' and contains(text(), 'See all ')]/..")
        )
        subsections_type_2 = response.css(".categoryRefinementsSection li a")

        subsections = subsections_type_1 if not subsections_type_2 else subsections_type_2

        to_return = {'subsections': subsections,
                     'type1': len(subsections_type_1) > 0,
                     'type2': len(subsections_type_2) > 0
                     }

        return to_return

    '''
    Obtiene el numero de paginas de productos que contiene una subseccion.
    '''
    @staticmethod
    def get_top_page(response):
        possible_top_page_1 = response.css(".pagnDisabled::text").extract()
        possible_top_page_2 = (response.xpath(
            '//*[@id="pagn"]/span[@class="pagnLink"][position()=last()]//text()'
        ).extract())

        if not possible_top_page_1:
            if not not possible_top_page_2:
                return int(possible_top_page_2[0])
            else:
                return 0
        else:
            return int(possible_top_page_1[0])

    '''
    Devuelve el listado de enlaces de productos de una pagina de productos.
    '''
    def get_products_urls(self, response):
        item = {}

        sel = scrapy.Selector(response)

        possible_product_links_list_1 = sel.css("#mainResults ul.s-result-list li a.s-access-detail-page")
        possible_product_links_list_2 = (sel.css(
            "#resultsCol ul li div.a-fixed-left-grid-col div.a-row.a-spacing-small a")
        )
        possible_product_links_list_3 = (sel.css(
            "#s-results-list-atf li div.s-item-container div.a-row.a-spacing-none div.a-row.a-spacing-mini > a")
        )

        links_list = (possible_product_links_list_1 if not possible_product_links_list_2
                      else possible_product_links_list_2 if not possible_product_links_list_3
                      else possible_product_links_list_3)

        links_list = [link.xpath('./@href')[0].extract() for link in links_list]

        item['list'] = ([(link + "?showDetailTechData=1").encode("utf-8") for link in links_list
                         if link.startswith("https://www.amazon.com/")])

        self._scrap_products(item['list'])

        return item

    '''
    Cuando se detecta que una seccion no es una subseccion final, se invoca este metodo para seguir explorando
    subsecciones en profundidad.
    '''
    def recursive_sections_getter(self, response):
        return scrapy.Request(response.url, self.parse)

    '''
    Analiza un producto dado un enlace
    '''
    @staticmethod
    def _scrap_products(product_links):
        scrapped_products = 0

        for link in product_links:
            call(["scrapy", "runspider", "amazon_product_scrapper.py", "-a", "start_url=%s" % link])
            scrapped_products += 1
            logging.info("Scrapped %d/%d products" % (scrapped_products, len(product_links)))
