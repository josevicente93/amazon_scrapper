# -*- encoding: utf-8 -*-
'''
Created on 6 Mar 2016

@author: jose
'''
import scrapy
import re
import mysql.connector

class Crawler(scrapy.Spider):
    #http://doc.scrapy.org/en/latest/intro/tutorial.html#following-links
    #scrapy runspider crawlerHyM.py

    name = 'hym'
    allowed_domains = ["hm.com"]
    base = "http://www2.hm.com/es_es"
    page_size = "&page-size=500"

    cnx = mysql.connector.connect(user='root', password='pass',
                                  host='127.0.0.1',
                                  database='tfgData')

    cursor = cnx.cursor()

    start_urls = [
        base + "/hombre/compra-por-producto/camisetas-de-manga-corta-y-sin-mangas.html?product-type=men_tshirtstanks&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/camisas.html?product-type=men_shirts&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/sudaderas-con-y-sin-capucha.html?product-type=men_hoodiessweatshirts&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/cardigans-y-jerseis.html?product-type=men_cardigansjumpers&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/americanas-y-trajes.html?product-type=men_blazerssuits&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/chaquetas-y-abrigos.html?product-type=men_jacketscoats&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/pantalones-y-chinos.html?product-type=men_trousers&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/vaqueros.html?product-type=men_jeans&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/pantalones-cortos.html?product-type=men_shorts&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/moda-de-bano.html?" + page_size,
        base + "/hombre/compra-por-producto/ropa-interior.html?product-type=men_underwear&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/calcetines.html?" + page_size,
        base + "/hombre/compra-por-producto/sport.html?product-type=men_sport&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/basics.html?product-type=men_basics&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/accesorios.html?" + page_size,
        base + "/hombre/compra-por-producto/calzado.html?product-type=men_shoes&sort=stock&offset=0" + page_size,
        base + "/hombre/compra-por-producto/tallas-amplias.html?product-type=men_extended_sizes&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/chaquetas-y-abrigos.html?product-type=ladies_jacketscoats&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/cardigans-y-jerseis.html?product-type=ladies_cardigansjumpers&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/tops.html?product-type=ladies_tops&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/camisas-y-blusas.html?product-type=ladies_shirtsblouses&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/basics.html?product-type=ladies_basics&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/vestidos.html?product-type=ladies_dresses&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/pantalones.html?product-type=ladies_trousers&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/vaqueros.html?product-type=ladies_jeans&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/ropa-para-dormir.html?product-type=ladies_nightwear&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/lenceria.html?product-type=ladies_lingerie&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/calcetines-y-medias.html?product-type=ladies_sockstights&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/accesorios.html?product-type=ladies_accessories&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/calzado.html?product-type=ladies_shoes&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/monos.html?product-type=ladies_jumpsuits&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/faldas.html?product-type=ladies_skirts&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/americanas-y-chalecos.html?product-type=ladies_blazerswaistcoats&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/h-m-sport.html?product-type=ladies_sport&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/pantalones-cortos.html?product-type=ladies_shorts&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/moda-de-bano.html?product-type=ladies_swimwear&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/beauty.html?product-type=ladies_beauty&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/h-m-tallas-grandes.html?product-type=ladies_plus&sort=stock&offset=0" + page_size,
        base + "/mujer/compra-por-producto/ropa-de-premama.html?product-type=ladies_maternity&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/recien-nacido.html?product-type=kids_newborn_all&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/bebes-ninas.html?product-type=kids_babygirl_all&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/bebes-ninos.html?product-type=kids_babyboy_all&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/ninas-15-10.html?product-type=kids_girl8y_all&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/ninos-15-10.html?product-type=kids_boy8y_all&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/ninas-8-14.html?product-type=kids_girl14y_all&sort=stock&offset=0" + page_size,
        base + "/ninos/compra-por-producto/ninos-8-14.html?product-type=kids_boys14y_all&sort=stock&offset=0" + page_size,
        base + "/home/compra-por-producto/cojines.html?product-type=home_cushions_all&sort=stock&offset=0" + page_size,
        base + "/home/compra-por-producto/ropa-de-cama.html?product-type=home_bedlinen_all&sort=stock&offset=0" + page_size,
        base + "/home/compra-por-producto/toallas.html?product-type=home_towels_all&sort=stock&offset=0" + page_size,
        base + "/home/compra-por-producto/alfombrillas-de-bano.html?" + page_size,
        base + "/home/compra-por-producto/cortinas-de-bano.html?" + page_size,
        base + "/home/compra-por-producto/cestas.html?product-type=home_storage_all&sort=stock&offset=0" + page_size,
        base + "/home/compra-por-producto/mantas.html?" + page_size,
        base + "/home/compra-por-producto/cortinas.html?" + page_size,
        base + "/home/compra-por-producto/decoracion.html?product-type=home_decorations&sort=stock&offset=0" + page_size,
        base + "/home/compra-por-producto/Velas-y-portavelas-desde.html?product-type=home_candlescandleholders&sort=stock&offset=0" + page_size
    ]

    def parse(self, response):
        for article in response.css('.product-item'):
            url = response.urljoin(article.css('h3 > a::attr(href)')[0].extract().encode("utf-8"))
            yield scrapy.Request(url, callback=self.parse_article)

    def parse_article(self, response):
        sel = scrapy.Selector(response)
        article_url = response.url #

        article_title = sel.css('.product-item-headline::text')[0].extract().encode("utf-8")  #
        article_price = float(sel.css('.price-value::text')[0].extract().encode("utf-8").replace(',', '.').replace(' ','').replace('€', '')) #
        colors = sorted(set(sel.css('.product-colors > ul li > label::attr(title)').extract()))
        article_colors = [unicode(color.encode('utf-8'), 'utf-8') for color in colors] #
        sizes = sorted(set(sel.css('.product-sizes > ul li > label > div > span::text').extract()))
        article_sizes = [unicode(size.encode('utf-8'), 'utf-8') for size in sizes] #
        article_id = re.search('http://www2.hm.com/es_es/productpage.(.*).html', article_url).group(1) #
        recommendations = sel.css("#style-with-article-code-"+article_id+" > section > div > div > ul li > div:nth-child(1) > article > h3 > a::attr(href)").extract()
        article_recommendations = [re.search('/es_es/productpage.(.*).html', recommendation).group(1) for recommendation in recommendations]
        people = sel.css('.product-items-content > .row > .swipe-items .swipe-item > .grid > .product-item > .product-item-headline > a::attr(href)').extract()
        article_other_people_also_bought = [re.search('/es_es/productpage.(.*).html', person).group(1) for person in people]
        article_description = sel.css('.product-detail-description-text::text')[0].extract().encode("utf-8") #
        article_category = re.search('http://www2.hm.com/es_es/(.*)/compra-por-producto', response.request.headers['referer']).group(1) #
        Crawler.cursor.execute("INSERT INTO hym VALUES ("+article_id+",'"+article_title+"','"+article_category+"',"+str(article_price)+",'"+article_url+"','"+article_description+"','"+str(article_colors).encode("utf-8").replace("u'", '').replace("'", '')+"','"+str(article_sizes).encode("utf-8").replace("u'", '').replace("'", '')+"','"+str(article_recommendations).encode("utf-8").replace("u'", '').replace("'", '')+"','"+str(article_other_people_also_bought).encode("utf-8").replace("u'", '').replace("'", '')+"');")
        item = {}
        item['id'] = article_id
        item['title'] = article_title
        item['category'] = article_category
        item['price'] = article_price
        item['url'] = article_url
        item['description'] = article_description
        item['colors'] = article_colors
        item['sizes'] = article_sizes
        item['recommendations'] = article_recommendations
        item['people'] = article_other_people_also_bought

        yield item

    def closed(self, reason):
        Crawler.cursor.execute("commit;")
        Crawler.cnx.close()