# -*- encoding: utf-8 -*-
'''
Created on 6 Mar 2016

@author: jose
'''
import scrapy
import mysql.connector
import scrapy.settings
import re

class Crawler(scrapy.Spider):
    #http://doc.scrapy.org/en/latest/intro/tutorial.html#following-links
    #scrapy runspider crawlerRegality.py

    #DOWNLOAD_DELAY = 5;

    name = 'regality'
    allowed_domains = ["regality.es"]

    cnx = mysql.connector.connect(user='root', password='pass',
                                  host='127.0.0.1',
                                  database='tfgData')

    cursor = cnx.cursor()

    start_urls = [
        "http://www.regality.es/82-todos?id_category=82&n=188"
    ]

    def parse(self, response):
        for article in response.css('.product_list .ajax_block_product > .product-container > div > div:nth-child(2) > div a::attr(href)').extract():
            url = article
            yield scrapy.Request(url, callback=self.parse_article)

    def parse_article(self, response):
        sel = scrapy.Selector(response)
        article_id = sel.css('#product_reference > span::text')[0].extract().encode("utf-8")  #
        article_url = response.url #
        article_title = sel.css('#center_column > div.primary_block.row.block > div.pb-right-column.col-xs-12.col-sm-6.col-md-7.col-lg-7 > h1::text')[0].extract().encode("utf-8")  #
        article_price = float(sel.css('#our_price_display::text')[0].extract().encode("utf-8").replace(',', '.').replace(' ','').replace('€', '')) #
        colors = sorted(set(sel.css('#color_to_pick_list li a::attr(title)').extract()))

        article_colors = [unicode(color.encode('utf-8'), 'utf-8') for color in colors] #
        article_recommendations = sel.css("#blockproductscategory > div > div.item > div > div > div > div.right-block > div > h5 > a::attr(href)").extract()
        p_text = ""
        for parragraph in sel.css('#center_column > section:nth-child(2) > div p::text').extract():
            p_text = p_text + " " + unicode(parragraph.encode('utf-8'), 'utf-8')
        article_category = re.search('http://www.regality.es/(.*)/', article_url).group(1)  #
        Crawler.cursor.execute("INSERT INTO regality VALUES ("+article_id+",'"+article_title+"','"+article_category+"',"+str(article_price)+",'"+article_url+"','"+p_text+"','"+str(article_colors).encode("utf-8").replace("u'", '').replace("'", '')+"','"+str(article_recommendations).encode("utf-8").replace("u'", '').replace("'", '')+"');")
        item = {}
        item['id'] = article_id
        item['title'] = article_title
        item['category'] = article_category
        item['price'] = article_price
        item['url'] = article_url
        item['description'] = p_text
        item['colors'] = article_colors
        item['recommendations'] = article_recommendations
        yield item

    def closed(self, reason):
        Crawler.cursor.execute("commit;")
        Crawler.cnx.close()